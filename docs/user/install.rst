.. _install:

Installation of xml driver
==========================
To include this driver into your project, the ``drb-driver-xml`` module shall
be referenced into requirement.txt file, or the following pip line can be run:

.. code-block::

    pip install drb-driver-xml
