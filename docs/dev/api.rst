.. _api:

Reference API
=============

XmlBaseNode
------------
.. autoclass:: drb.drivers.xml.node.XmlBaseNode
    :members:

XmlNode
------------
.. autoclass:: drb.drivers.xml.node.XmlNode
    :members:

extract_namespace_name
----------------------
.. autoclass:: drb.drivers.xml.node.extract_namespace_name

