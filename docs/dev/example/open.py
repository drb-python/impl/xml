from xml.etree.ElementTree import fromstring

from drb.drivers.xml import XmlNode

path_file = 'path/to_your/xml.xml'
str_xml = f'''
        <f:foobar xmlns:f='something'>
            <f:foo>foobar_1</f:foo>
            <f:foo>foobar_2</f:foo>
            <f:foo>foobar_3</f:foo>
            <bar>foobar_4</bar>
            <bar>foobar_5</bar>
        </f:foobar>
        '''
# Load xml from string
node = XmlNode(fromstring(str_xml))

# Get the number of children (here 5)
len(node.children)

# the name of the second children here 'foo'
node.children[2].name

# Get the namespace
node.children[2].namespace_uri

# Get the value 'foobar_3'
node.children[2].value

# Get the name of the 5 children 'bar'
node.children[4].name

# The value of 'foobar_5'
node.children[4].value

# Get all the attributes of the node
node[4].attributes