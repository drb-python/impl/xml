===================
Data Request Broker
===================
------------------
XML driver for DRB
------------------
Release v\ |version|.


.. image:: https://pepy.tech/badge/drb-driver-xml/month
    :target: https://pepy.tech/project/drb-driver-xml
    :alt: Requests Downloads Per Month Badge

.. image:: https://img.shields.io/pypi/l/drb-driver-xml.svg
    :target: https://pypi.org/project/drb-driver-xml/
    :alt: License Badge

.. image:: https://img.shields.io/pypi/wheel/drb-driver-xml.svg
    :target: https://pypi.org/project/drb-driver-xml/
    :alt: Wheel Support Badge

.. image:: https://img.shields.io/pypi/pyversions/drb-driver-xml.svg
    :target: https://pypi.org/project/drb-driver-xml/
    :alt: Python Version Support Badge

-------------------

This drb-driver-xml module implements xml format access with DRB data model.
It is able to navigates among the xml contents.

User Guide
==========

.. toctree::
   :maxdepth: 2

   user/install


.. toctree::

   dev/api

Example
=======

.. toctree::
   :maxdepth: 2

   dev/example

