from drb.core.factory import FactoryLoader
from drb.core.item_class import ItemClassLoader, ItemClassType
from drb.nodes.logical_node import DrbLogicalNode
from drb.drivers.xml import XmlNodeFactory
import unittest
import uuid


class TestDrbImageSignature(unittest.TestCase):
    mock_pkg = None
    fc_loader = None
    ic_loader = None
    xml_ic_id = uuid.UUID('40123218-2b5e-11ec-8d3d-0242ac130003')

    @classmethod
    def setUpClass(cls) -> None:
        cls.fc_loader = FactoryLoader()
        cls.ic_loader = ItemClassLoader()

    def test_impl_loading(self):
        factory_name = 'xml'

        factory = self.fc_loader.get_factory(factory_name)
        self.assertIsNotNone(factory)
        self.assertIsInstance(factory, XmlNodeFactory)

        item_class = self.ic_loader.get_item_class(self.xml_ic_id)
        self.assertIsNotNone(factory)
        self.assertEqual(self.xml_ic_id, item_class.id)
        self.assertEqual('Extensible Markup Language', item_class.label)
        self.assertIsNone(item_class.description)
        self.assertEqual(ItemClassType.FORMATTING, item_class.category)
        self.assertEqual(factory, item_class.factory)

    def test_impl_signatures(self):
        item_class = self.ic_loader.get_item_class(self.xml_ic_id)

        node = DrbLogicalNode('foobar.xml')
        self.assertTrue(item_class.matches(node))

        node = DrbLogicalNode('FOOBAR.XML')
        self.assertTrue(item_class.matches(node))

        node = DrbLogicalNode('content')
        node.add_attribute('Content-Type', 'application/xml')
        self.assertTrue(item_class.matches(node))

        node = DrbLogicalNode('foobar.xsd')
        self.assertFalse(item_class.matches(node))

        node = DrbLogicalNode('content')
        node.add_attribute('Content-Type', 'application/json')
        self.assertFalse(item_class.matches(node))
