from io import BytesIO, BufferedIOBase
from drb.core.node import DrbNode
from drb.core.factory import DrbFactory
from drb.nodes.logical_node import DrbLogicalNode


class MemoryNode(DrbLogicalNode):
    def __init__(self, name: str, content: bytes):
        super(MemoryNode, self).__init__(name)
        self.content = content

    def has_impl(self, impl: type) -> bool:
        if issubclass(impl, BufferedIOBase):
            return True
        return False

    def get_impl(self, impl: type, **kwargs):
        return BytesIO(self.content)


class DummyFactory(DrbFactory):
    def _create(self, node: DrbNode) -> DrbNode:
        return node
