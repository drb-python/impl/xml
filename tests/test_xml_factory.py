from drb.nodes.logical_node import DrbLogicalNode
from drb.drivers.xml import XmlNodeFactory
from .utils import MemoryNode
import sys
import unittest
import os
import tempfile


class TestXmlFactoryNode(unittest.TestCase):
    xml = """
    <fb:foobar xmlns:fb="https://foobar.org/foobar"
               xmlns:f="https://foobar.org/foo">
        <f:foo>3</f:foo>
        <bar xmlns="https://foobar.org/bar">
            <tata>something</tata>
            <f:tutu>else</f:tutu>
        </bar>
        <f:bar>Hello</f:bar>
        <f:bar>Hello</f:bar>
    </fb:foobar>
    """
    path = None
    invalid_path = None
    file_node = None

    @classmethod
    def setUpClass(cls) -> None:
        path = os.path.dirname(__file__)
        mock_pkg = os.path.abspath(os.path.join(path, 'resources'))
        sys.path.append(mock_pkg)

        fd, cls.path = tempfile.mkstemp(suffix='.xml', text=True)
        with os.fdopen(fd, 'w') as file:
            file.write(cls.xml)
            file.flush()
        fd, cls.invalid_path = tempfile.mkstemp(suffix='.txt', text=True)
        os.close(fd)

    @classmethod
    def tearDownClass(cls) -> None:
        os.remove(cls.path)
        os.remove(cls.invalid_path)

    def test_create_from_path(self):
        path = os.path.join(os.path.abspath(
            os.path.dirname(__file__)), 'resources/test.xml')

        node = XmlNodeFactory().create(path)

        self.assertIsNotNone(node)
        self.assertEqual(node.name, 'root')
        self.assertEqual(len(node), 3)

        child = node[0]
        self.assertEqual(child.name, 'elem')
        self.assertIsNone(child.namespace_uri)
        self.assertEqual(child.value, '4')

        node.close()

    def test_create_from_node(self):
        content = b'<root><elem>1</elem><elem>2</elem><elem>3</elem></root>'
        child_name = 'memory_data'
        parent = DrbLogicalNode('parent')
        child = MemoryNode(child_name, content)
        parent.append_child(child)
        node = XmlNodeFactory().create(child)

        self.assertIsNotNone(node)
        self.assertEqual(child_name, node.name)
        self.assertEqual(f'parent/{child_name}', node.path.name)
        self.assertEqual(len(node), 1)
        root = node[0]
        self.assertEqual('root', root.name)
        self.assertEqual(f'parent/{child_name}/root', root.path.name)
        self.assertEqual(len(root), 3)

        node.close()

    def test_create_from_string_xml_content(self):
        node = XmlNodeFactory().create(self.xml)
        self.assertEqual('foobar', node.name)
        self.assertEqual('https://foobar.org/foobar', node.namespace_uri)

        n = node[0]
        self.assertEqual('foo', n.name)
        self.assertEqual('https://foobar.org/foo', n.namespace_uri)

        n = node[1]
        self.assertEqual('bar', n.name)
        self.assertEqual('https://foobar.org/bar', n.namespace_uri)
        self.assertEqual(2, len(n))

        n = node[1][0]
        self.assertEqual('tata', n.name)
        self.assertEqual('https://foobar.org/bar', n.namespace_uri)

        n = node[1][1]
        self.assertEqual('tutu', n.name)
        self.assertEqual('https://foobar.org/foo', n.namespace_uri)

        self.assertNotEqual(hash(node[2]), hash(node[3]))
        node.close()
